from django.db import models


class Book(models.Model):
    name = models.CharField(max_length=20, null=False)
    title = models.CharField(max_length=30, null=True, blank=True)
    author = models.CharField(max_length=30, null=False)
    description = models.CharField(max_length=512, null=True, blank=True)
    price = models.IntegerField(max_length=5, null=False)


class Profile(models.Model):
    column_name = models.CharField(max_length=30, null=False)
    is_visible = models.BooleanField(default=True)