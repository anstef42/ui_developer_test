from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.validation import FormValidation
from django import forms

import models


class BookForm(forms.ModelForm):
    class Meta:
        model = models.Book
        fields = ['name', 'title', 'author', 'description', 'price']


class BookResource(ModelResource):
    class Meta:
        queryset = models.Book.objects.all()
        resource_name = 'book'
        authorization = Authorization()
        validation = FormValidation(form_class=BookForm)
        fields = ['name', 'title', 'author', 'description', 'price']


class ProfileResource(ModelResource):
    column_name = fields.CharField(attribute='column_name', readonly=True)
    is_visible = fields.BooleanField(attribute='is_visible')

    class Meta:
        queryset = models.Profile.objects.all()
        resource_name = 'profile'
        authorization = Authorization()
        allowed_methods = ['get', 'patch', 'put']
