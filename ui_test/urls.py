from django.conf.urls import patterns, include, url
from tastypie.api import Api

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from books.api import BookResource, ProfileResource

book_resource = BookResource()
profile_resource = ProfileResource()

v1_api = Api(api_name='v1')
v1_api.register(BookResource())
v1_api.register(ProfileResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ui_test.views.home', name='home'),
    # url(r'^ui_test/', include('ui_test.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^api/', include(v1_api.urls)),
)
